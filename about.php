<?php
session_start();
include("Include/db.php");
include("Function/function.php")
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Handicraft</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/form.css" rel="stylesheet" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery1.min.js"></script>
<!-- start menu -->
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!--start slider -->
    <link rel="stylesheet" href="css/fwslider.css" media="all">
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/css3-mediaqueries.js"></script>
    <script src="js/fwslider.js"></script>
<!--end slider -->
<script src="js/jquery.easydropdown.js"></script>

</head>
<body>
<?php cart();?>
     <div class="header-top">
	   <div class="wrap"> 
			  <div class="header-top-left">
			  	   <div class="box">
			  	   <a href="index.html"><img src="images/logo.jpg" width="150px" height="50px" alt="" /></a>
   				     <!-- <select tabindex="4" class="dropdown">
							<option value="" class="label" value="">Language :</option>
							<option value="1">English</option>
							<option value="2">Nepali</option>
					  </select>-->
   				    </div>
   				    <div class="box1">
   				        <!--<select tabindex="4" class="dropdown">
							<option value="" class="label" value="">Currency :</option>
							<option value="1">$ Dollar</option>
							<option value="2">€ Euro</option>
						</select>-->
   				    </div>
   				    <div class="clear"></div>
   			 </div>
			 <div class="cssmenu">
				<ul>
					<li class="active grid"><a href="index.php">Home</a></li>|
					<li><a href="customer_login.php">Account</a></li> |
					<li><a href="paypal/index.php">Cart</a></li> |
					<li> 
					<?php
          if (!isset($_SESSION['customer_email'])) {
            echo "<li><a href='checkout.php'  style=
            'text-decoration: none; color: white;'
            ></span > Login</a></li>";
          }
          else
          {
            echo "<li><a href='logout.php'  style='text-decoration: none; color: white;'> Logout</a></li>";
          }
      ?>
					</li> |
					<li><a href="register.php">Sign Up</a></li>
					<li>
         <div class="search">	  
         <form action="result.php" enctype="multipart/form-data">
				<input type="text" name="user_query" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
				<input type="submit" value="search" id="submit" name="search">
				<div id="response"> </div></form></div>
				</li>
				</ul>
			</div>
			<div class="clear"></div>
 		</div>
	</div>
	<div class="header-bottom">
	    <div class="wrap">
			<div class="row">
				<div class="logo">
					<!--<a href="index.html"><img src="images/logo.jpg" width="90px" height="90px" alt=""/></a>-->
				</div>
				<div class="menu">
	            <ul class="megamenu skyblue">
			
				<?php
				$category_list = categorylist();
				for ($i=0; $i < sizeof($category_list); $i++) { 
					$sub_category_section = subcategory($category_list[$i]['id']);
					if(sizeof($sub_category_section) > 0){
					?>
					<li><a class="color4" href="#"><?= $category_list[$i]['title']?></a>
						<div class="megapanel">
							<div class="row">
								<div class="col1">
									<div class="h_nav">

										<ul style=" -moz-column-count: 3;
											    -moz-column-gap: 10px;
											    -webkit-column-count: 3;
											    -webkit-column-gap: 10px;
											    column-count: 2;
											    column-gap: 10px;">
										<?php
											for ($k=0; $k < sizeof($sub_category_section) ; $k++) { 
												?>
													<li>
													<a href="paypal/index.php?cats=<?= $category_list[$i]['id']; ?>&sub_cat=<?= $sub_category_section[$k]['id']; ?>"><?= $sub_category_section[$k]['title']; ?></a>
													</li>


												
												<?php
											}
										?>
										</ul>	
									</div>							
								</div>
							  </div>
							</div>
						</li>
					<?php
					}else{
						?>
						<li><a href="paypal/index.php?cats=<?= $category_list[$i]['id']; ?>"><?= $category_list[$i]['title']; ?></a></li>
						<?php
					}
				}

			?>		
	</ul>
</div>
		</div>
	   <!--<div class="header-bottom-right">
         <div class="search">	  
         <form action="result.php" enctype="multipart/form-data">
				<input type="text" name="user_query" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
				<input type="submit" value="search" id="submit" name="search">
				<div id="response"> </div>
		 </div>
	  <div class="tag-list">
	    <ul class="icon1 sub-icon1 profile_img">
			<li><a class="active-icon c1" href="#"> </a>
				<ul class="sub-icon1 list">
					<li><h3>sed diam nonummy</h3><a href=""></a></li>
					<li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>
				</ul>
			</li>
		</ul>
		<ul class="icon1 sub-icon1 profile_img">
			<li><a class="active-icon c2" href="#"> </a>
				<ul class="sub-icon1 list">
					<li><h3>No Products</h3><a href=""></a></li>
					<li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>
				</ul>
			</li>
		</ul>
	    <ul class="last"><li><a href="cart.php">Cart(<?php  //total_iteams(); ?>)</a></li></ul>
	  </div>-->
    </div>
     <div class="clear"></div>
     </div>
	</div>

       <div class="login">
          <div class="wrap">
				<ul class="breadcrumb breadcrumb__t"><a class="home" href="index.php">Home</a>  / About</ul>
				<div class="section group">
				   <div class="labout span_1_of_about">
					 <h3>Testimonials</h3>
					  <div class="testimonials ">
						<div class="testi-item">
						<blockquote class="testi-item_blockquote">
							<a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </a>
							<div class="clear"></div>
						</blockquote>
							<small class="testi-meta"><span class="user">Lorem ipsum dolor </span>,
							<span class="info">sit amet</span><br><a href="#">http://demolink.org</a></small></div>
					   </div>
					   <div class="testimonials ">
						<div class="testi-item">
						<blockquote class="testi-item_blockquote">
							<a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </a>
							<div class="clear"></div>
						</blockquote>
							<small class="testi-meta"><span class="user">Lorem ipsum dolor</span>,
							<span class="info">sit amet</span><br><a href="#">http://demolink.org</a></small></div>
					   </div>
				    </div>
				    <div class="cont span_2_of_about">
				       <h3>VISSION AND MISSION</h3>
					   	<p>AA Handicrafts is a movement of producers wholesalers, retailers and advocates working to promote social economic and environmental justice. Some of the principles of fair trade include fair wages, Environmental sustainable practices, safe and healthy working conditions, financial and technical support, long term relationship, and respect for cultural identity. At the deepest essence, our mission is to create a direct link between you and the many talented artisans in Nepal.</p>
					   	<h3>ETHICAL POLICY</h3>
					   	<p>
					   	AA Handicrafts support and promote fair trade principles in all aspects of the business. We only sell our products from suppliers who source products from local artists, communities and craftsmen. We believe that producers should be representative of the religions and communities of the area and both male and female craftsmen are employed through the various stages of production. AA Handicrafts believe that education is critical to any community. As such child labor is not tolerated. we visit the producers that we work with regularly to inspect production conditions and to maintain personal relationships with the craftsmen. We aim to ensure that products are made from sustainable and legal raw materials through carefully chosen suppliers. All shipping materials should be recyclable. We aim to recycle and re-use wherever possible and are supplied with green energy.

					   	</p>
					   	<h3>CULTURAL CRAFTS</h3>
					   	<p>
					   			Nepal’s cultural craft heritage is particularly impressive considering the range of different crafts and the range of different culturally identifiable artisan communities. For example, Nepal is famous for its Wood Carving, Cotton Cloth, Nepalese paper, Tibetan Handicraft, Tibetan Incense, Buddhist and Hindu statue, woolen item and the particularly unique costumes and jewelry of Tibetan Design and Nepalese Design. These artistic traditions are sometimes divided into groups.
					   	</p>
				  
							
		   </div>
		   <!-- Add fancyBox main JS and CSS files -->
		<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
		<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
		<script>
			$(document).ready(function() {
				$('.popup-with-zoom-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
			});
		});
		</script>
           </div>
		   <div class="clear"></div>	
		  </div>
	</div>	
   </div>  
    <div class="footer">
		<div class="footer-top">
			<div class="wrap">
			  <div class="section group example">
				<div class="col_1_of_2 span_1_of_2">
					<ul class="f-list">
					  <li><img src="images/2.png"><span class="f-text">Free Shipping on orders over $500</span><div class="clear"></div></li>
					</ul>
				</div>
				<div class="col_1_of_2 span_1_of_2">
					<ul class="f-list">
					  <li><img src="images/3.png"><span class="f-text">Call us! +977-01-4253504</span><div class="clear"></div></li>
					</ul>
				</div>
				<div class="clear"></div>
		      </div>
			</div>
		</div>
		<div class="footer-middle">
			<div class="wrap">
			 <!-- <div class="section group">
			  	<div class="f_10">
					<div class="col_1_of_4 span_1_of_4">
						<h3>Facebook</h3>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						<div class="like_box">	
							<div class="fb-like-box" data-href="http://www.facebook.com/w3layouts" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
						</div>
					</div>
					<div class="col_1_of_4 span_1_of_4">
						<h3>From Twitter</h3>
						<div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
								<p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
							</div>
							<div class="clear"> </div>
						</div>
						<div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
								<p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
							</div>
							<div class="clear"> </div>
						</div>
					</div>
				</div>
				<div class="f_10">
					<div class="col_1_of_4 span_1_of_4">
					    <h3>Information</h3>
						<ul class="f-list1">
						    <li><a href="#">Duis autem vel eum iriure </a></li>
				            <li><a href="#">anteposuerit litterarum formas </a></li>
				            <li><a href="#">Tduis dolore te feugait nulla</a></li>
				             <li><a href="#">Duis autem vel eum iriure </a></li>
				            <li><a href="#">anteposuerit litterarum formas </a></li>
				            <li><a href="#">Tduis dolore te feugait nulla</a></li>
			         	</ul>
					</div>
					<div class="col_1_of_4 span_1_of_4">
						<h3>Contact us</h3>
						<div class="company_address">
					                <p>500 Lorem Ipsum Dolor Sit,</p>
							   		<p>22-56-2-9 Sit Amet, Lorem,</p>
							   		<p>USA</p>
					   		<p>Phone:(00) 222 666 444</p>
					   		<p>Fax: (000) 000 00 00 0</p>
					 	 	<p>Email: <span>mail[at]leoshop.com</span></p>
					   		
					   </div>
					   <div class="social-media">
						     <ul>
						        <li> <span class="simptip-position-bottom simptip-movable" data-tooltip="Google"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Linked in"><a href="#" target="_blank"> </a> </span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Rss"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Facebook"><a href="#" target="_blank"> </a></span></li>
						    </ul>
					   </div>
					</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		  </div>-->
		   
		   
		   
		   
		   
		   <div class="section group example">
			  <div class="col_1_of_f_1 span_1_of_f_1">
				 <div class="section group example">
				   <div class="col_1_of_f_2 span_1_of_f_2">
				      <h3>Facebook</h3>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						<div class="like_box">	
							<div class="fb-like-box" data-href="https://www.facebook.com/aathamel/" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
						</div>
 				  </div>
				  <div class="col_1_of_f_2 span_1_of_f_2">
						<h3>From Twitter</h3>
				       <div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
								
							</div>
							<div class="clear"> </div>
					   </div>
					   <div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
				
							</div>
							<div class="clear"> </div>
					  </div>
				</div>
				<div class="clear"></div>
		      </div>
 			 </div>
			 <div class="col_1_of_f_1 span_1_of_f_1">
			   <div class="section group example">
				 <div class="col_1_of_f_2 span_1_of_f_2">
				    <h3>Information</h3>
						<ul class="f-list1">
						    <li><a href="#"><img src="images/logo.jpg" width="150" height="100"> </a></li>
				            <li><a href="#">H.NO 11 JP Road Thamel Kathmandu </a></li>
				            <li><a href="#">+977-01-4253604/9801071676/9851071671</a></li>
				             <li><a href="#">Map</a></li>
				            <li><a href="#">aathamle@gmail.com</a></li>
			         	</ul>
 				 </div>
				 <div class="col_1_of_f_2 span_1_of_f_2">
				   <h3>Contact us</h3>
						<div class="company_address">
					                <p>Om Mantra Craft</p>
							   		<p>MRetailer/Wholesaler</p>
							   		<p>H.NO 11 JP Road Thamel Kathmandu </p>
					   		<p>Phone:(+977)01-4253604</p>
					   		<p>USA: 347-393-3640</p>
					 	 	<p>Email: <span>mail[at]aathamel@gmail.com</span></p>
					 	 	<p>Webside: <span>www.ommantracraft.com</span></p>
					   		
					   </div>
					   <div class="social-media">
						     <ul>
						        <li> <span class="simptip-position-bottom simptip-movable" data-tooltip="Google"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Linked in"><a href="#" target="_blank"> </a> </span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Rss"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Facebook"><a href="#" target="_blank"> </a></span></li>
						    </ul>
					   </div>
				</div>
				<div class="clear"></div>
		    </div>
		   </div>
		  <div class="clear"></div>
		    </div>
		  </div>
		</div>
		<div class="footer-bottom">
			<div class="wrap">
	             <div class="copy">
			        <p>© 2017 www.ommantracraft.com<a href="http://www.aahandicraft.com" target="_blank">Om Mantra Craft</a></p>
		         </div>
				<div class="f-list2">
				 <ul>
					<li class="active"><a href="about.php">About Us</a></li> |
					<li><a href="delivery.php">Delivery & Returns</a></li> |
					<li><a href="Terms.php">Terms & Conditions</a></li> |
					<li><a href="contact.php">Contact Us</a></li> 
				 </ul>
			    </div>
			    <div class="clear"></div>
		      </div>
	     </div>
	</div>
</body>
</html>