<html lang="en">
<head>
    <title>PHP - Paypal Payment Gateway Integration</title>
</head>
<body style="background:#E1E1E1">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
<style type="text/css">
		.price.panel-red>.panel-heading {
			color: #fff;
			background-color: #D04E50;
			border-color: #FF6062;
			border-bottom: 1px solid #FF6062;
		}	
		.price.panel-red>.panel-body {
			color: #fff;
			background-color: #EF5A5C;
		}
		.price .list-group-item{
			border-bottom-:1px solid rgba(250,250,250, .5);
		}
		.panel.price .list-group-item:last-child {
			border-bottom-right-radius: 0px;
			border-bottom-left-radius: 0px;
		}
		.panel.price .list-group-item:first-child {
			border-top-right-radius: 0px;
			border-top-left-radius: 0px;
		}
		.price .panel-footer {
			color: #fff;
			border-bottom:0px;
			background-color:  rgba(0,0,0, .1);
			box-shadow: 0px 3px 0px rgba(0,0,0, .3);
		}
		.panel.price .btn{
			box-shadow: 0 -1px 0px rgba(50,50,50, .2) inset;
			border:0px;
		}
</style>
<?php
include("Include/db.php");
include("Function/function.php");
$total=0;
	global $con;
	$ip=getIp();
	$sel_price="select *from cart where ip_add='$ip'";
	$run_price=mysqli_query($con,$sel_price);
	while ($p_price=mysqli_fetch_array($run_price)) {
		$pro_id=$p_price['p_id'];
		$pro_price="select * from product where product_id='$pro_id'";
		$run_pro_price=mysqli_query($con,$pro_price);
		while ($pp_price=mysqli_fetch_array($run_pro_price)) {
			$product_price=array($pp_price['product_price']);
			$product_name=$pp_price['product_title'];
			$valuse=array_sum($product_price);
			$total +=$valuse;
		}
	}
	printf($total);
	exit();
	//getting qty from cart table
$get_qty="select * from cart where p_id=$pro_id";
$run_qty=mysqli_query($con,$get_qty);
$row_qty=mysqli_fetch_array($run_qty);
$qty=$row_qty['qty'];
if ($qty==0) {
	$qty=1;
}
else{
	$qty=$qty;
}

	$paypalUrl='https://www.sandbox.paypal.com/cgi-bin/webscr';
	$paypalId='kvs3944-facilitator@gmail.com';
?>
 
<div class="container text-center">
	<br/>
	<h2><strong>Pay Now With PayPal</strong></h2>
	<br/>
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-3 col-md-offset-4 col-lg-3">
		
			<!-- PRICE ITEM -->
    			<form action="<?php echo $paypalUrl; ?>" method="post" name="frmPayPal1">
					<div class="panel price panel-red">
						    <input type="hidden" name="business" value="<?php echo $paypalId; ?>">
						    <input type="hidden" name="cmd" value="_xclick">
						    <input type="hidden" name="item_name" value="<?php echo '$product_name';?>">
						    <input type="hidden" name="item_number" value="<?php echo '$qty';?>">
						    <input type="hidden" name="amount" value="<?php echo '$total';?>">
						    <input type="hidden" name="no_shipping" value="1">
						    <input type="hidden" name="currency_code" value="USD">
						    <input type="hidden" name="cancel_return" value="http://aahandicraft.com/cancel.php">
						    <input type="hidden" name="return" value="http://aahandicraft.com/success.php">  
						    
						<div class="panel-heading  text-center">
						
							<button class="btn btn-lg btn-block btn-danger" href="#" >BUY NOW!</button>
					</div>
    			</form>
			<!-- /PRICE ITEM -->
			
		</div>
	</div>
</div>
</body>
</html>