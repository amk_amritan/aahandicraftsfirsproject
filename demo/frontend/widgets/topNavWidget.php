<?php 
namespace frontend\widgets;
use yii\base\widget;
use yii\helpers\Html;
use frontend\models\menu;
use frontend\models\caption;
class topNavWidget extends widget
{
	public $message;
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		$query =menu::find()->all();

		return $this->render('topNavWidget', [
            'menus' => $query,
            ]);
	}
}

?>