<div class="projects">
   <div class="container">
    <h3 class="tittle">Recent <span>Projects</span></h3>
	  <div class="projects-inner">
	    <div class="col-md-7 banner-slider">
							<div class="callbacks_container">
								<ul class="rslides" id="slider3">
									<li>
									  <div class="blog-img">
										 <img src="images/pro1.jpg" class="img-responsive" alt="" />
									  </div>
								
									</li>
									<li>
									  <div class="blog-img">
										<img src="images/pro2.jpg" class="img-responsive" alt="" />
									  </div>
								
								  </li>
								  <li>
									  <div class="blog-img">
										<img src="images/pro3.jpg" class="img-responsive" alt="" />
									  </div>
									   
								  </li>
								  <li>
									  <div class="blog-img">
										<img src="images/pro4.jpg" class="img-responsive" alt="" />
									  </div>
							
								  </li>
								</ul>
						  </div>
					</div>
					<div class="col-md-5 ban-text">
					   <div class="choose">
							<div class="choose_img">
								 <h3>Why choose us ?</h3>
								   <!-- choose icon -->
								   <div class="choose_icon">
										<div class="choose_left">
											<span class="glyphicon glyphicon-home"></span>
										</div>
											<div class="choose_right">
												<p>Lorem Ipsum is simply dummy text of the printing and
												typesetting industry. Lorem Ipsum </p>
											</div>
										<div class="clearfix"></div>
									 </div>
								 
								  <!-- choose icon -->
								  <div class="choose_icon">
									<div class="choose_left">
										<span class="glyphicon glyphicon-pencil"></span>
									</div>
									<div class="choose_right">
										<p>Lorem Ipsum is simply dummy text of the printing and
										typesetting industry. Lorem Ipsum </p>
									</div>
									<div class="clearfix"></div>
								 </div>
								 
								  <!-- choose icon -->
								  <div class="choose_icon">
									<div class="choose_left">
										<span class="glyphicon glyphicon-heart-empty"></span>
									</div>
									<div class="choose_right">
										<p>Lorem Ipsum is simply dummy text of the printing and
										typesetting industry. Lorem Ipsum </p>
									</div>
									<div class="clearfix"></div>
								 </div> 
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
					<!--banner Slider starts Here-->
					<script src="js/responsiveslides.min.js"></script>
						 <script>
							// You can also use "$(window).load(function() {"
							$(function () {
							  // Slideshow 3
							  $("#slider3").responsiveSlides({
								auto: true,
								pager:false,
								nav: true,
								speed: 500,
								namespace: "callbacks",
								before: function () {
								  $('.events').append("<li>before event fired.</li>");
								},
								after: function () {
								  $('.events').append("<li>after event fired.</li>");
								}
							  });
						
							});
						  </script>
		 </div>
		 </div>