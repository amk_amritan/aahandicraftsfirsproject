
<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>


<section id="line-slider">
            <div class="leoslider">
              <ul class="slides">
              <?php foreach ($slider as $slider):?>
                <li>
                <?php 
                $slider_image=$slider->slider_image;

                ?>
                   <img src="<?php echo Yii::$app->request->baseUrl.'/../../demo/backend/'.$slider_image?>" alt=" " />
                </li>  
                <?php endforeach; ?>        
              </ul>
        
            </div>
        </section>
        <!-- /Line Slider -->
</div>
            <div class="banner-bottom">
                <!--//screen-gallery-->
                        <div class="sreen-gallery-cursual">

                               <div id="owl-demo" class="owl-carousel">
                                  <div class="item-owl">
                                            <div class="test-review">
                                              <img src="frontend/web/images/oman.jpg" class="img-responsive" alt=""/>
                                              <h5>Oman</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                        <div class="test-review">
                                              <img src="frontend/web/images/Malaysia-icon.png" class="img-responsive" alt=""/>
                                              <h5>Malaysia</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="frontend/web/images/Flag_of_United_Arab_Emirates.png" class="img-responsive" alt=""/>
                                              <h5>United Arab Emirates</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="frontend/web/images/quater.png" class="img-responsive" alt=""/>
                                              <h5>Quater</h5>
                                            </div>
                                    </div>
                                    <div class="item-owl">
                                            <div class="test-review">
                                              <img src="frontend/web/images/baharin.png" class="img-responsive" alt=""/>
                                              <h5>Baharin</h5>
                                            </div>
                                    </div>
                                    
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="frontend/web/images/polant.jpg" class="img-responsive" alt=""/>
                                              <h5>Poland</h5>
                                            </div>
                                    </div>
                                     
                                  
                                     
                              </div>
                        <!--//screen-gallery-->

          </div>
 </div>
 <!-- about-type-grid -->
    <div class="about" id="about">
                    <div class="col-md-5 ab-left">
                                <div class="btm-right">
                                    <a href="single.html">
                                        <img src="frontend/web/images/c5.jpg" alt="" class="img-responsive">
                                        <div class="captn">
                                            
                                                            
                                        </div>
                                    </a>    
                            </div>

                            
                        </div>

        <div class="col-md-7 ab-right">
                      <div class="col-bottom two">
                            <h2 class="tittle">Welcome to our <span>Dream Recruiting service Pvt.Ltd</span></h2>
                            <h4>We Build your Dream </h4>
                            <p>
Our "Objective" and "Aim" is to provide our clients with "Comprehensive Solutions" for your organization's manpower requirements. We strive to get a "Perfect Match" of employees for our potential customers and leave no stone un-turned or do not mind going the extra mile to achieve desired results for our customers.</p>
                            <a href="single.html" class="hvr-shutter-in-horizontal">Read More</a>
                        </div>

        </div>
        <div class="clearfix"> </div>
    </div>
 
<!-- //about-type-grid -->
<div class="projects">
   <div class="container">
    <h3 class="tittle">Recent <span>News</span></h3>
    <div class="projects-inner">
      <div class="col-md-7 banner-slider">
              <div class="callbacks_container">
              <ul class="rslides" id="slider3">
              <?php foreach ($project as $recentproject): ?>
              <?php  $project_image=$recentproject->recentproject_image?>
                  <li>
                    <div class="blog-img">
                     <img src="<?php echo Yii::$app->request->baseUrl.'/../../demo/backend/web/'.$project_image?>" class="img-responsive" alt="" />
                    </div>
                
                  </li>
                  <?php endforeach;?>
                  
                  
                </ul>
              </div>
          </div>
          <div class="col-md-5 ban-text">
             <div class="choose">
              <div class="choose_img">
                 <h3>Why choose us ?</h3>
                   <!-- choose icon -->
                   <div class="choose_icon">
                    <div class="choose_left">
                      <span class="glyphicon glyphicon-home"></span>
                    </div>
                      <div class="choose_right">
                        <p>Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum </p>
                      </div>
                    <div class="clearfix"></div>
                   </div>
                 
                  <!-- choose icon -->
                  <div class="choose_icon">
                  <div class="choose_left">
                    <span class="glyphicon glyphicon-pencil"></span>
                  </div>
                  <div class="choose_right">
                    <p>Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum </p>
                  </div>
                  <div class="clearfix"></div>
                 </div>
                 
                  <!-- choose icon -->
                  <div class="choose_icon">
                  <div class="choose_left">
                    <span class="glyphicon glyphicon-heart-empty"></span>
                  </div>
                  <div class="choose_right">
                    <p>Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum </p>
                  </div>
                  <div class="clearfix"></div>
                 </div> 
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
<!-- service-type-grid -->
    <div class="service" id="services">
            <div class="feature-grids">
                
                <div class="col-md-7 feature-grid">
                <h3 class="tittle three">Our <span>Services</span></h3>
                    
        <?php foreach ($service as $service):?>
            <?php $service_image=$service->service_image ?>
            <?php $id=$service->service_id ?>



                <div class="feature">
                        <div class="features-item sans-shadow text-center">
                            <div class="features-icon">
                                <div class="hi-icon-wrap hi-icon-effect-9 hi-icon-effect-9a">
                                    <a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=site/single&id='.$id?>" class="hi-icon"><img src="<?php echo Yii::$app->request->baseUrl.'/../../demo/backend/'.$service_image?>"  alt=" " /></a>
                                </div>
                            </div>
                            <div class="features-info">
                                <h4><?= Html::encode("{$service->service_title}") ?></h4>
                            <p><?= Html::encode("{$service->service_desc}") ?></p>
                            </div>
                        </div>
                    </div>
                    

             <?php endforeach; ?>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-5 feature-grid-image">
                    <div class="btm-right">
                                    <a href="single.html">
                                        <img src="frontend/web/images/jobs in Bahrain.jpg" alt="" class="img-responsive">
                                        <div class="captn">
                                            
                                                            
                                        </div>
                                    </a>    
                            </div>
                </div>
                <div class="clearfix"></div>

            </div>
    </div>
<!-- //service-type-grid -->
<!--news-->
  

<div class="news-section" id="news">
      <div class="container">
         <h3 class="tittle">Recent <span>News</span></h3>
        <div class="news-left">   
        <?php foreach ($news as $recentnews):?>
            <?php $news_image=$recentnews->recentnews_sm_image ?>
              <?php $news_imageone=$recentnews->recentnews_sm_imageone ?> 
              <?php $id=$recentnews->recentnews_id ?> 
          <div class="col-md-6 col-news-right">
            <div class="col-news-top">
              <a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=site/single&id='.$id?>" class="date-in">
                <img class="img-responsive mix-in" src="<?php echo Yii::$app->request->baseUrl.'/../../demo/backend/web/'.$news_image?>" alt="">
                <div class="month-in">
                  <label>
                  <span class="day">30</span>
                  <span class="month">March</span>
                  <span class="like">We Build your Dreams</span>
                  </label>
                </div>
              </a>
              <div class="clearfix"> </div>
              <div class="col-bottom">
              <h4><?=Html::encode("{$recentnews->recentnews_title}")?></h4>
                            <p><?=Html::encode("{$recentnews->recentnews_stdesc}")?> </p>
              <a href="single.html" class="hvr-shutter-in-horizontal">Read More</a>
            </div>
            </div>  
            
          </div>
          <div class="col-md-6 col-news">
            <div class="col-bottom two">
              <h4><?=Html::encode("{$recentnews->recentnews_titleone}")?></h4>
              <p><?=Html::encode("{$recentnews->recentnews_stdescone}")?> </p>
              <a href="single.html" class="hvr-shutter-in-horizontal">Read More</a>
            </div>
            <div class="col-news-top">
            
              <a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=site/single&id='.$id?>" class="date-in">
                <img class="img-responsive mix-in" src="<?php echo Yii::$app->request->baseUrl.'/../../demo/backend/web/'.$news_imageone?>" alt="">
                <div class="month-in">
                  <label>
                  <span class="day">30</span>
                  <span class="month">August</span>
                  <span class="like">We Build your Dreams</span>
                  </label>
                </div>
              </a>
              <div class="clearfix"> </div>
              </div> 
            </div>          
           
          <div class="clearfix">
        </div><?php endforeach; ?>
      </div>
     
    </div>

    
    <!--//news-->
    <div class="product" id="product">
        <div class="container">
            <div class="product-top">
                <div class="col-md-4 product-left heading">
                    <h3 class="tittle new">New <span>Arrivals</span></h3>
                    <ul>
                    <?php foreach ($newarrivals as $newarrivals):?>
                      <?php $id=$newarrivals->newarrivals_id ?>
                        <li><i class="glyphicon glyphicon-arrow-right"></i><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=site/single&id='.$id?>"><?=Html::encode("{$newarrivals->newarrivals_title}")?></a></li>
                       <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-8 product-right heading">
                <h3 class="tittle new">New<span>Products</span></h3>
                    
                <?php foreach ($newproduct as $product):?>
                        
                        <?php $product_image=$product->product_small_image  ?>
                        <?php $id=$product->product_id ?>


                        <div class="prdt">
                        <div class="col-md-6 prdt-left">
                            <div class="btm-right1">
                                    <a href="single.html">
                                        <img src="<?php echo Yii::$app->request->baseUrl.'/../../demo/backend/web/'.$product_image?>" class="img-responsive">
                                        <div class="captn1">
                                            <h5><?=Html::encode("{$product->product_title}")?></h5>
                                                            
                                        </div>
                                    </a>    
                            </div>
                            <a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=site/single&id='.$id?>"><h4><?=Html::encode("{$product->product_title}")?></h4></a>
                            <p><?=Html::encode("{$product->product_stdesc}")?></p>
                        </div>



<?php endforeach; ?>

                        
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--client-->
    <div class="client" id="client">
        <div class="container">
          
                <div class="customer-say">
                  <h3 class="tittle two">Customer <span>Says</span></h3>
                   <div class="col-md-6 customer-grid">
                        <div class="de_testi">
                                <div class="quotes"><img src="frontend/web/images/t1.jpg" alt=""></div>
                                    
                                     <div class="de_testi_by">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                                         <a href="#">Michael </a>, Customer
                                    </div>
                                    <div class="clearfix"></div>
                                   
                            </div>
                   
                   </div>
                   <div class="col-md-6 customer-grid">
                   
                   <div class="de_testi">
                                <div class="quotes"><img src="frontend/web/images/t2.jpg" alt=""></div>
                                    
                                     <div class="de_testi_by">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                                         <a href="#">John </a>, Customer
                                    </div>
                                    <div class="clearfix"></div>
                                   
                            </div>
                   
                   </div>
                </div>
        </div>
    </div>  
    <!--//client-->

<!--/timer-->
  <div class="time-bg">
         <h4>WE ARE <span>HEALPING </span> YOU</h4>
      <p id="demo">11:20:09 AM</p>
      
        <script>
        var myVar=setInterval(function(){myTimer()},1000);

        function myTimer() {
          var d = new Date();
          document.getElementById("demo").innerHTML = d.toLocaleTimeString();
        }
        </script>
       </div>
        <!--//timer-->


        
        <?php

      echo $this->registerJsFile('@web/js/first.js');
      echo $this->registerJsFile('@web/js/second.js');
      echo $this->registerJsFile('@web/js/third.js');
      echo $this->registerJsFile('@web/js/fourth.js');
      echo $this->registerJsFile('@web/js/fifth.js');
        ?>