<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/frontend/web';
    public $css = [
        'css/site.css',
        'css/iconeffects.css',
        'css/style.css',
        'css/leoslider.css',
        'css/iconeffects.css',
        '//fonts.googleapis.com/css?family=Montserrat:400,700',
        '//fonts.googleapis.com/css?family=Open+Sans:400,600',
        '//fonts.googleapis.com/css?family=Nosifer',
        'css/owl.carousel.css',
        'css/bootstrap.css',
    ];
    public $js = [
        'js/move-top.js',
        'js/easing.js',
        'js/jquery.leoslider.js',
        'js/main.js',
        'js/owl.carousel.js',
        'js/responsiveslides.min.js',
        'js/bootstrap.js',
        'js/first.js',
        'js/second.js',
        'js/third.js',
        'js/fourth.js',
        'js/fifth.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',

    ];
}
