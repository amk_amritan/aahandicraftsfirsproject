<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "slidersecond".
 *
 * @property integer $slidersecond_id
 * @property string $slidersecond_title
 * @property string $slidersecond_image
 */
class Slidersecond extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'slidersecond';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slidersecond_title'], 'required'],
            [['file'],'file'],
            [['slidersecond_title'], 'string'],
            [['slidersecond_image'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'slidersecond_id' => 'Slidersecond ID',
            'slidersecond_title' => 'Slidersecond Title',
            'file' => 'Slidersecond Image',
        ];
    }
}
