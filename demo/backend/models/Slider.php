<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $slider_id
 * @property string $slider_title
 * @property string $slider_image
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_title'], 'required'],
            [['file'],'file'],
            [['slider_title'], 'string'],
            [['slider_image'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'slider_id' => 'Slider ID',
            'slider_title' => 'Slider Title',
            'file' => 'Slider Image',
        ];
    }
}
