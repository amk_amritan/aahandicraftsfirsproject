<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recentproject".
 *
 * @property integer $recentproject_id
 * @property string $recentproject_title
 * @property string $recentproject_image
 */
class Recentproject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'recentproject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recentproject_title'], 'required'],
            [['file'],'file'],
            [['recentproject_title'], 'string'],
            [['recentproject_image'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recentproject_id' => 'Recentproject ID',
            'recentproject_title' => 'Recentproject Title',
            'file' => 'Recentproject Image',
        ];
    }
}
