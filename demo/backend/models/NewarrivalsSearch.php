<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Newarrivals;

/**
 * NewarrivalsSearch represents the model behind the search form about `backend\models\Newarrivals`.
 */
class NewarrivalsSearch extends Newarrivals
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newarrivals_id'], 'integer'],
            [['newarrivals_title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newarrivals::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'newarrivals_id' => $this->newarrivals_id,
        ]);

        $query->andFilterWhere(['like', 'newarrivals_title', $this->newarrivals_title]);

        return $dataProvider;
    }
}
