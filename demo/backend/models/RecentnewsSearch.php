<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Recentnews;

/**
 * RecentnewsSearch represents the model behind the search form about `backend\models\Recentnews`.
 */
class RecentnewsSearch extends Recentnews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recentnews_id'], 'integer'],
            [['recentnews_title', 'recentnews_stdesc', 'recentnews_lndesc', 'recentnews_sm_image', 'recentnews_sm_image_name', 'recentnews_big_image', 'recentnews_titleone', 'recentnews_stdescone', 'recentnews_lndescone', 'recentnews_sm_imageone', 'recentnews_big_imageone_name', 'recentnews_big_imageone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recentnews::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recentnews_id' => $this->recentnews_id,
        ]);

        $query->andFilterWhere(['like', 'recentnews_title', $this->recentnews_title])
            ->andFilterWhere(['like', 'recentnews_stdesc', $this->recentnews_stdesc])
            ->andFilterWhere(['like', 'recentnews_lndesc', $this->recentnews_lndesc])
            ->andFilterWhere(['like', 'recentnews_sm_image', $this->recentnews_sm_image])
            ->andFilterWhere(['like', 'recentnews_sm_image_name', $this->recentnews_sm_image_name])
            ->andFilterWhere(['like', 'recentnews_big_image', $this->recentnews_big_image])
            ->andFilterWhere(['like', 'recentnews_titleone', $this->recentnews_titleone])
            ->andFilterWhere(['like', 'recentnews_stdescone', $this->recentnews_stdescone])
            ->andFilterWhere(['like', 'recentnews_lndescone', $this->recentnews_lndescone])
            ->andFilterWhere(['like', 'recentnews_sm_imageone', $this->recentnews_sm_imageone])
            ->andFilterWhere(['like', 'recentnews_big_imageone_name', $this->recentnews_big_imageone_name])
            ->andFilterWhere(['like', 'recentnews_big_imageone', $this->recentnews_big_imageone]);

        return $dataProvider;
    }
}
