<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $service_id
 * @property string $service_title
 * @property string $service_desc
 * @property string $service_image
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_title', 'service_desc'], 'required'],
            [['file'],'file'],
            [['service_title', 'service_desc'], 'string'],
            [['service_image'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'service_title' => 'Service Title',
            'service_desc' => 'Service Desc',
            'file' => 'Service Image',
        ];
    }
}
