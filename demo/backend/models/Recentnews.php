<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recentnews".
 *
 * @property integer $recentnews_id
 * @property string $recentnews_title
 * @property string $recentnews_stdesc
 * @property string $recentnews_lndesc
 * @property string $recentnews_sm_image
 * @property string $recentnews_sm_image_name
 * @property string $recentnews_big_image
 * @property string $recentnews_titleone
 * @property string $recentnews_stdescone
 * @property string $recentnews_lndescone
 * @property string $recentnews_sm_imageone
 * @property string $recentnews_big_imageone_name
 * @property string $recentnews_big_imageone
 */
class Recentnews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public $files;
    public $file1;
    public $files1;
    public static function tableName()
    {
        return 'recentnews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recentnews_title', 'recentnews_stdesc', 'recentnews_lndesc', 'recentnews_titleone', 'recentnews_stdescone', 'recentnews_lndescone'], 'required'],
            [['file','files','file1','files1'],'file'],
            [['recentnews_title', 'recentnews_stdesc', 'recentnews_lndesc', 'recentnews_sm_image_name', 'recentnews_titleone', 'recentnews_stdescone', 'recentnews_lndescone', 'recentnews_big_imageone_name'], 'string'],
            [['recentnews_sm_image', 'recentnews_big_image'], 'string', 'max' => 225],
            [['recentnews_sm_imageone', 'recentnews_big_imageone'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recentnews_id' => 'Recentnews ID',
            'recentnews_title' => 'Recentnews Title',
            'recentnews_stdesc' => 'Recentnews Stdesc',
            'recentnews_lndesc' => 'Recentnews Lndesc',
            'file' => 'Recentnews Sm Image',
            'recentnews_sm_image_name' => 'Recentnews Sm Image Name',
            'files' => 'Recentnews Big Image',
            'recentnews_titleone' => 'Recentnews Titleone',
            'recentnews_stdescone' => 'Recentnews Stdescone',
            'recentnews_lndescone' => 'Recentnews Lndescone',
            'file1' => 'Recentnews Sm Imageone',
            'recentnews_big_imageone_name' => 'Recentnews Big Imageone Name',
            'files1' => 'Recentnews Big Imageone',
        ];
    }
}
