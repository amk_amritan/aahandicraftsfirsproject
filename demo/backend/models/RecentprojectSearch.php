<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Recentproject;

/**
 * RecentprojectSearch represents the model behind the search form about `backend\models\Recentproject`.
 */
class RecentprojectSearch extends Recentproject
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recentproject_id'], 'integer'],
            [['recentproject_title', 'recentproject_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recentproject::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recentproject_id' => $this->recentproject_id,
        ]);

        $query->andFilterWhere(['like', 'recentproject_title', $this->recentproject_title])
            ->andFilterWhere(['like', 'recentproject_image', $this->recentproject_image]);

        return $dataProvider;
    }
}
