<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "newarrivals".
 *
 * @property integer $newarrivals_id
 * @property string $newarrivals_title
 */
class Newarrivals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newarrivals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newarrivals_title'], 'required'],
            [['newarrivals_title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newarrivals_id' => 'Newarrivals ID',
            'newarrivals_title' => 'Newarrivals Title',
        ];
    }
}
