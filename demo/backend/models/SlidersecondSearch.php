<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Slidersecond;

/**
 * SlidersecondSearch represents the model behind the search form about `backend\models\Slidersecond`.
 */
class SlidersecondSearch extends Slidersecond
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slidersecond_id'], 'integer'],
            [['slidersecond_title', 'slidersecond_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Slidersecond::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'slidersecond_id' => $this->slidersecond_id,
        ]);

        $query->andFilterWhere(['like', 'slidersecond_title', $this->slidersecond_title])
            ->andFilterWhere(['like', 'slidersecond_image', $this->slidersecond_image]);

        return $dataProvider;
    }
}
