<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $product_id
 * @property string $product_title
 * @property string $product_stdesc
 * @property string $product_lndesc
 * @property string $product_small_image
 * @property string $product_big_image
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public $files;
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_title', 'product_stdesc', 'product_lndesc'], 'required'],
            [['file','files'],'file'],
            [['product_title', 'product_stdesc', 'product_lndesc'], 'string'],
            [['product_small_image', 'product_big_image'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_title' => 'Product Title',
            'product_stdesc' => 'Product Stdesc',
            'product_lndesc' => 'Product Lndesc',
            'file' => 'Product Small Image',
            'files' => 'Product Big Image',
        ];
    }
}
