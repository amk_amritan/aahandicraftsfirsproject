<?php

namespace backend\controllers;

use Yii;
use backend\models\Recentnews;
use backend\models\RecentnewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * RecentnewsController implements the CRUD actions for Recentnews model.
 */
class RecentnewsController extends Controller
{
    /**
     * @inheritdoc
     */
     public function behaviors()
    {
        return [
          'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','_search','_form','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recentnews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecentnewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recentnews model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recentnews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recentnews();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $imageName=$model->recentnews_title;
            $model->file=UploadedFile::getInstance($model,'file');
            $model->file->saveAs( 'product-image/'.$imageName.'.'.$model->file->extension);
            $model->recentnews_sm_image='product-image/'.$imageName.'.'.$model->file->extension;


             $imageName1=$model->recentnews_sm_image_name;
            $model->files=UploadedFile::getInstance($model,'files');
            $model->files->saveAs( 'product-image/'.$imageName1.'.'.$model->files->extension);
            $model->recentnews_big_image='product-image/'.$imageName1.'.'.$model->files->extension;



             $imageName2=$model->recentnews_titleone;
            $model->file1=UploadedFile::getInstance($model,'file1');
            $model->file1->saveAs( 'product-image/'.$imageName2.'.'.$model->file1->extension);
            $model->recentnews_sm_imageone='product-image/'.$imageName2.'.'.$model->file1->extension;


             $imageName3=$model->recentnews_big_imageone_name;
            $model->files1=UploadedFile::getInstance($model,'files1');
            $model->files1->saveAs( 'product-image/'.$imageName3.'.'.$model->files1->extension);
            $model->recentnews_big_imageone='product-image/'.$imageName3.'.'.$model->files1->extension;
            $model->save();
            return $this->redirect(['view', 'id' => $model->recentnews_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Recentnews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->recentnews_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Recentnews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Recentnews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recentnews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recentnews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
