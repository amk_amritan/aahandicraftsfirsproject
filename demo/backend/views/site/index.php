<?php

/* @var $this yii\web\View */

$this->title = 'DEMO PROJECT';
?>
<div class="site-index">


    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2>MENU</h2>

                <p><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=menu'?>"><img src="<?php echo Yii::$app->request->baseUrl.'/web/icon/menu.png'?>"></a></p>

                
            </div>
            <div class="col-lg-3">
                <h2>SLIDER</h2>

                <p><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=slider'?>"><img src="<?php echo Yii::$app->request->baseUrl.'/web/icon/slider.png'?>"></a></p>
            </div>

            <div class="col-lg-3">
                <h2>SECOND SLIDER</h2>

                <p><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=slidersecond'?>"><img src="<?php echo Yii::$app->request->baseUrl.'/web/icon/slider.png'?>"></a></p>

                
            </div>
            <div class="col-lg-3">
                <h2>OUR SERVICES</h2>

                <p><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=service'?>"><img src="<?php echo Yii::$app->request->baseUrl.'/web/icon/Services.png'?>"></a></p>
            </div>
            <div class="row">
                <div class="col-lg-3">
                <h2>NEW ARRIVALS</h2>

                <p><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=newarrivals'?>"><img src="<?php echo Yii::$app->request->baseUrl.'/web/icon/newitems.jpg'?>"></a></p>

                
            </div>
            <div class="col-lg-3">
                <h2>RECENT PROJECT</h2>

                <p><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=recentproject'?>"><img src="<?php echo Yii::$app->request->baseUrl.'/web/icon/project.png'?>"></a></p>
            </div>

            <div class="col-lg-3">
                <h2>RECENT NEWS</h2>

                <p><a href="<?php echo Yii::$app->request->baseUrl.'/index.php?r=recentnews'?>"><img src="<?php echo Yii::$app->request->baseUrl.'/web/icon/news.jpg'?>"></a></p>

                
            </div>
        </div>

    </div>
</div>
