<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Recentproject */

$this->title = 'Update Recentproject: ' . $model->recentproject_id;
$this->params['breadcrumbs'][] = ['label' => 'Recentprojects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->recentproject_id, 'url' => ['view', 'id' => $model->recentproject_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recentproject-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
