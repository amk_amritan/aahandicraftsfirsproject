<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Recentproject */

$this->title = $model->recentproject_id;
$this->params['breadcrumbs'][] = ['label' => 'Recentprojects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recentproject-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->recentproject_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->recentproject_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'recentproject_id',
            'recentproject_title:ntext',
            'recentproject_image',
        ],
    ]) ?>

</div>
