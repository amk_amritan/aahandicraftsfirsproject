<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Recentproject */

$this->title = 'Create Recentproject';
$this->params['breadcrumbs'][] = ['label' => 'Recentprojects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recentproject-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
