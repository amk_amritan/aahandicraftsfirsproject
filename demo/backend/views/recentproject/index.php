<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RecentprojectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recentprojects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recentproject-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Recentproject', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'recentproject_id',
            'recentproject_title:ntext',
            'recentproject_image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
