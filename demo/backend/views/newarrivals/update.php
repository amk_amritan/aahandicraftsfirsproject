<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Newarrivals */

$this->title = 'Update Newarrivals: ' . $model->newarrivals_id;
$this->params['breadcrumbs'][] = ['label' => 'Newarrivals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->newarrivals_id, 'url' => ['view', 'id' => $model->newarrivals_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="newarrivals-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
