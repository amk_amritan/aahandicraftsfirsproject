<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Newarrivals */

$this->title = 'Create Newarrivals';
$this->params['breadcrumbs'][] = ['label' => 'Newarrivals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newarrivals-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
