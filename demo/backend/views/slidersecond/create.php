<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Slidersecond */

$this->title = 'Create Slidersecond';
$this->params['breadcrumbs'][] = ['label' => 'Sliderseconds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slidersecond-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
