<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SlidersecondSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sliderseconds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slidersecond-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Slidersecond', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'slidersecond_id',
            'slidersecond_title:ntext',
            'slidersecond_image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
