<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Slidersecond */

$this->title = $model->slidersecond_id;
$this->params['breadcrumbs'][] = ['label' => 'Sliderseconds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slidersecond-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->slidersecond_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->slidersecond_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'slidersecond_id',
            'slidersecond_title:ntext',
            'slidersecond_image',
        ],
    ]) ?>

</div>
