<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Slidersecond */

$this->title = 'Update Slidersecond: ' . $model->slidersecond_id;
$this->params['breadcrumbs'][] = ['label' => 'Sliderseconds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->slidersecond_id, 'url' => ['view', 'id' => $model->slidersecond_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="slidersecond-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
