<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Recentnews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recentnews-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'recentnews_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'recentnews_stdesc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'recentnews_lndesc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'recentnews_sm_image_name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'files')->fileInput() ?>

    <?= $form->field($model, 'recentnews_titleone')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'recentnews_stdescone')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'recentnews_lndescone')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file1')->fileInput() ?>

    <?= $form->field($model, 'recentnews_big_imageone_name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'files1')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
