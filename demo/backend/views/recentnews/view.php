<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Recentnews */

$this->title = $model->recentnews_id;
$this->params['breadcrumbs'][] = ['label' => 'Recentnews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recentnews-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->recentnews_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->recentnews_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'recentnews_id',
            'recentnews_title:ntext',
            'recentnews_stdesc:ntext',
            'recentnews_lndesc:ntext',
            'recentnews_sm_image',
            'recentnews_sm_image_name:ntext',
            'recentnews_big_image',
            'recentnews_titleone:ntext',
            'recentnews_stdescone:ntext',
            'recentnews_lndescone:ntext',
            'recentnews_sm_imageone',
            'recentnews_big_imageone_name:ntext',
            'recentnews_big_imageone',
        ],
    ]) ?>

</div>
