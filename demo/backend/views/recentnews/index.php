<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RecentnewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recentnews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recentnews-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Recentnews', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'recentnews_id',
            'recentnews_title:ntext',
            //'recentnews_stdesc:ntext',
            //'recentnews_lndesc:ntext',
            //'recentnews_sm_image',
            // 'recentnews_sm_image_name:ntext',
            // 'recentnews_big_image',
            'recentnews_titleone:ntext',
            // 'recentnews_stdescone:ntext',
            // 'recentnews_lndescone:ntext',
            // 'recentnews_sm_imageone',
            // 'recentnews_big_imageone_name:ntext',
            // 'recentnews_big_imageone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
