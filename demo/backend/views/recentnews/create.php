<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Recentnews */

$this->title = 'Create Recentnews';
$this->params['breadcrumbs'][] = ['label' => 'Recentnews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recentnews-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
