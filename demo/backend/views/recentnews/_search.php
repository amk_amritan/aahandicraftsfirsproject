<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RecentnewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recentnews-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'recentnews_id') ?>

    <?= $form->field($model, 'recentnews_title') ?>

    <?= $form->field($model, 'recentnews_stdesc') ?>

    <?= $form->field($model, 'recentnews_lndesc') ?>

    <?= $form->field($model, 'recentnews_sm_image') ?>

    <?php // echo $form->field($model, 'recentnews_sm_image_name') ?>

    <?php // echo $form->field($model, 'recentnews_big_image') ?>

    <?php // echo $form->field($model, 'recentnews_titleone') ?>

    <?php // echo $form->field($model, 'recentnews_stdescone') ?>

    <?php // echo $form->field($model, 'recentnews_lndescone') ?>

    <?php // echo $form->field($model, 'recentnews_sm_imageone') ?>

    <?php // echo $form->field($model, 'recentnews_big_imageone_name') ?>

    <?php // echo $form->field($model, 'recentnews_big_imageone') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
