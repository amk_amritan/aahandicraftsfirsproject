<?php
session_start();
 ?>
<?php
include("Include/db.php");
include("Function/function.php");
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Handicraft| Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery1.min.js"></script>
<!-- start menu -->
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- dropdown -->
<script src="js/jquery.easydropdown.js"></script>
</head>
<body>
    <div class="header-top">
      <div class="wrap"> 
        <div class="header-top-left">
             <div class="box">
                <!--<select tabindex="4" class="dropdown">
              <option value="" class="label" value="">Language :</option>
              <option value="1">English</option>
              <option value="2">French</option>
              <option value="3">German</option>
            </select>-->
              </div>
              <div class="box1">
                 <!-- <select tabindex="4" class="dropdown">
              <option value="" class="label" value="">Currency :</option>
              <option value="1">$ Dollar</option>
              <option value="2">€ Euro</option>
            </select>-->
              </div>
              <div class="clear"></div>
         </div>
       <div class="cssmenu">
        <ul>
          <li class="active"><a href="login.html">Account</a></li> |
          <li><a href="checkout.html">Wishlist</a></li> |
          <li><a href="checkout.html">Checkout</a></li> |
          <li><a href="login.html">Log In</a></li> |
          <li><a href="register.html">Sign Up</a></li>
        </ul>
      </div>
      <div class="clear"></div>
    </div>
   </div>
  <div class="header-bottom">
      <div class="wrap">
      <div class="header-bottom-left">
        <div class="logo">
          <a href="index.html"><img src="images/logo.jpg" alt=""/></a>
        </div>
        <div class="menu">
              <ul class="megamenu skyblue">
      <li class="active grid"><a href="index.html">Home</a></li>
      <li><a class="color4" href="#">women</a>
        <div class="megapanel">
          <div class="row">
            <div class="col1">
              <div class="h_nav">
                <h4>Contact Lenses</h4>
                <ul>
                  <li><a href="womens.html">Daily-wear soft lenses</a></li>
                  <li><a href="womens.html">Extended-wear</a></li>
                  <li><a href="womens.html">Lorem ipsum </a></li>
                  <li><a href="womens.html">Planned replacement</a></li>
                </ul> 
              </div>              
            </div>
            <div class="col1">
              <div class="h_nav">
                <h4>Sun Glasses</h4>
                <ul>
                  <li><a href="womens.html">Heart-Shaped</a></li>
                  <li><a href="womens.html">Square-Shaped</a></li>
                  <li><a href="womens.html">Round-Shaped</a></li>
                  <li><a href="womens.html">Oval-Shaped</a></li>
                </ul> 
              </div>              
            </div>
            <div class="col1">
              <div class="h_nav">
                <h4>Eye Glasses</h4>
                <ul>
                  <li><a href="womens.html">Anti Reflective</a></li>
                  <li><a href="womens.html">Aspheric</a></li>
                  <li><a href="womens.html">Bifocal</a></li>
                  <li><a href="womens.html">Hi-index</a></li>
                  <li><a href="womens.html">Progressive</a></li>
                </ul> 
              </div>                        
            </div>
            </div>
          </div>
        </li>       
        <li><a class="color5" href="#">Men</a>
        <div class="megapanel">
          <div class="col1">
              <div class="h_nav">
                <h4>Contact Lenses</h4>
                <ul>
                  <li><a href="mens.html">Daily-wear soft lenses</a></li>
                  <li><a href="mens.html">Extended-wear</a></li>
                  <li><a href="mens.html">Lorem ipsum </a></li>
                  <li><a href="mens.html">Planned replacement</a></li>
                </ul> 
              </div>              
            </div>
            <div class="col1">
              <div class="h_nav">
                <h4>Sun Glasses</h4>
                <ul>
                  <li><a href="mens.html">Heart-Shaped</a></li>
                  <li><a href="mens.html">Square-Shaped</a></li>
                  <li><a href="mens.html">Round-Shaped</a></li>
                  <li><a href="mens.html">Oval-Shaped</a></li>
                </ul> 
              </div>              
            </div>
            <div class="col1">
              <div class="h_nav">
                <h4>Eye Glasses</h4>
                <ul>
                  <li><a href="mens.html">Anti Reflective</a></li>
                  <li><a href="mens.html">Aspheric</a></li>
                  <li><a href="mens.html">Bifocal</a></li>
                  <li><a href="mens.html">Hi-index</a></li>
                  <li><a href="mens.html">Progressive</a></li>
                </ul> 
              </div>                        
            </div>
          </div>
        </li>
        <li><a class="color6" href="other.html">Other</a></li>
        <li><a class="color7" href="other.html">Purchase</a></li>
      </ul>
      </div>
    </div>
     <div class="header-bottom-right">
         <div class="search">   
        <input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
        <input type="submit" value="Subscribe" id="submit" name="submit">
        <div id="response"> </div>
     </div>
    <div class="tag-list">
      <ul class="icon1 sub-icon1 profile_img">
      <li><a class="active-icon c1" href="#"> </a>
        <ul class="sub-icon1 list">
          <li><h3>sed diam nonummy</h3><a href=""></a></li>
          <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>
        </ul>
      </li>
    </ul>
    <ul class="icon1 sub-icon1 profile_img">
      <li><a class="active-icon c2" href="#"> </a>
        <ul class="sub-icon1 list">
          <li><h3>No Products</h3><a href=""></a></li>
          <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>
        </ul>
      </li>
    </ul>
      <ul class="last"><li><a href="#">Cart(0)</a></li></ul>
    </div>
    </div>
     <div class="clear"></div>
     </div>
  </div>
<!DOCTYPE html>
<head>
   <title>Payment Successfuly!</title>
   <script type="text/javascript">
        (function (global) { 

    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {            
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };          
    }

})(window);
    </script>
</head>
<body>
 <?php
 //this is all for product details
$total=0;
   global $con;
   $ip=getIp();
   $sel_price="select *from cart where ip_add='$ip'";
   $run_price=mysqli_query($con,$sel_price);
   $user=$_SESSION['customer_email'];
   $get_c="select *from customer where customer_email='$user'";
   $run_c=mysqli_query($con,$get_c);
   $row_c=mysqli_fetch_array($run_c);
   $C_id=$row_c['customer_id'];
   $c_email=$row_c['customer_email'];
   $c_name=$row_c['customer_name'];

$i=0;
   while ($p_price=mysqli_fetch_array($run_price)) {
      $pro_id=$p_price['p_id'];
      $qty=$p_price['qty'];

$pro_price="select * from product where product_id='$pro_id'";
      $run_pro_price=mysqli_query($con,$pro_price);
      $j=0;
while ($product = mysqli_fetch_array($run_pro_price)) {
      $product_price=$product['product_price'];
         $pro_name=$product['product_title'];
         $j++;
}
if ($qty==0) {
   $qty=1;
   $total=$product_price*$qty;
}
else{
   $qty=$qty;
   $total=$product_price*$qty;
}     
      



$currency="";
   $trx_id="";
   $amount=250;
   $invoice="";
   //inserting payment into paynment table
   $insert_payments="INSERT INTO payments (amount,customer_id,product_id,trx_id,currency,payment_date) VALUES('$total','$C_id','$pro_id','$trx_id','$currency',NOW())";
    $run_payment=mysqli_query($con,$insert_payments);
    //inserting order into order table
   $insert_order="INSERT INTO orders (p_id,c_id,qty,order_date,status,invoice_no) VALUES('$pro_id','$C_id','$qty',NOW(),'In Progress','$invoice')";
   $run_order=mysqli_query($con,$insert_order); 
   $empty_cart="delete from cart";
   $run_cart=mysqli_query($con,$empty_cart);

$i++;
}
?>
<?php 
   if($amount==$total){
   	echo "<h2>Welcome:-".$_SESSION['customer_email']."Your payment was successful! </h2>";
   	echo "<a href='user_account/index.php'>Go Your Account</a>";
   }
   else{
   	echo "<h2>Welcome Guest Payment is Successfuly</h2>";
   	echo "<a href='index.php'>Go Your Back to shop</a>";
   }
$headers="MIME-Version:1.1"."\r\n";
$headers="content-type:text/html;charset=UTF-8"."\r\n";
$headers='from: <sales@gmail.com>'."\r\n";
$subject="Order Details";
$message="<html>
<p> Hello Dear<b style='color:blue'>$c_name</b>You have order some product on our webside handicraft. Find your order details Bellow we can procced your order. thak you!</p>
<table width='600' align='center' bgcolor='#FFCCC99' border='2'> 
<tr><td><h2>Your Order Details from handicraft.com</h2></td></tr>
<tr>
<th><b>SN</b></th>
<th><b>Product</b></th>
<th<b>Quantity</b></th>
<th><b>Total Price</b></th>
<th><b>Invoice No</b></th>
</tr>
<tr> 
<td>$pro_name</td>
<td>$qty</td>
<td>$total</td>
<td>$invoice</td>

</tr>
</table>
<h3>Please Go to your account</h3>
<h2><a href='handicraft'>Click here</a>To login your Account<h2>
<h3>Thank you for order@-www.handicraft.com</h3>
</html>
" ;
mail($c_email, $subject,$message, $headers)
 ?>


</body>
</html>